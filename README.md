Данный проект был реализован в качестве тестового задания.

Описание приложения:
Мобильное приложение имеет два типа пользователей - заказчик и исполнитель(тип выбирается при авторизации).
Заказчики могут оставлять задания для исполнителей. Для этого они заходят в свой личный кабинет и там создают новое задание. 
В личном кабинете можно просмотреть все свои задания. 
Задание  имеет следующие поля Заголовок, Описание, Срок выполнения (дата и время), адрес выполнения и Стоимость. 

Исполнитель в своём личном кабинете может просматривать все задания всех заказчиков.
Перейдя в карточку задания, Исполнитель может откликнуться на данное задание.
Тогда у Заказчика в карточке задания, которое он создавал, появятся отклики от этих Исполнителей.

Добавлена возможность привязывать задание к конкретной точке , а в кабинете исполнителя просматривать и выбирать все задания при помощи карты. (приложение имеет подпись в режиме release)

Так же хочу отметить что приложение работает с реальной базой данных, а API сервера написано на php (разработкой занимался сам, но прошу не судить строго т.к это не моя специализация), связь с сервером осуществляется при помощи POST запросов, а ответ формируется в виде json строки.

![1.png](https://bitbucket.org/repo/p5kjrp/images/3685696733-1.png)![2.png](https://bitbucket.org/repo/p5kjrp/images/3396572301-2.png)![3.png](https://bitbucket.org/repo/p5kjrp/images/2419772992-3.png)

![4.png](https://bitbucket.org/repo/p5kjrp/images/2253212243-4.png)![5.png](https://bitbucket.org/repo/p5kjrp/images/824335627-5.png)![6.png](https://bitbucket.org/repo/p5kjrp/images/520062699-6.png)