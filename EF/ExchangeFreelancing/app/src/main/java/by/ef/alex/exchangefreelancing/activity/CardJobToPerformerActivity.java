package by.ef.alex.exchangefreelancing.activity;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import by.ef.alex.exchangefreelancing.GetJobApplication;
import by.ef.alex.exchangefreelancing.service.ManagerProgresDialog;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.service.ManagerSaveDate;
import by.ef.alex.exchangefreelancing.utils.Utils;


/**
 * класс реализует карточку задания для ИСПОЛНИТЕЛЯ
 */

public class CardJobToPerformerActivity extends Activity implements View.OnClickListener {

    private final String TRUE_TEXT = "true";
    private final String ERROR_CONNECT = "error_connect";

    private final String LIST_JOB = "listJob";
    private final String TITLE = "title";
    private final String COST = "cost";
    private final String ADDRES = "addres";
    private final String DESCRIPTION = "description";
    private final String DEAD_LINE_TIME = "deadLineTime";
    private final String RESPONSE = "response";

    private final String ERROR_RESPONSE = "response";


    public static final String JOB_ID = "jobID";
    public static String LOG_TAG = "myLog";


    private LinearLayout btnRespondToJob;
    private TextView textBtnResponse;
    private TextView textTitle;
    private TextView textCost;
    private TextView textAddres;
    private TextView textDescription;
    private TextView textDeadLineDate;

    private LinearLayout downloadArea;

    private ManagerProgresDialog mpDialog;

    private int jobID;

    /**
     * метод вызывает инициализацию activity, и передает в нее параметры
     */

    public static void start(Context context, long idJob) {

        Intent intent = new Intent(context, CardJobToPerformerActivity.class);
        intent.putExtra(JOB_ID, idJob);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_job_to_performer);

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(new ColorDrawable(0));

        downloadArea = (LinearLayout)findViewById(R.id.downloadArea);
        downloadArea.setVisibility(View.GONE);

        btnRespondToJob = (LinearLayout) findViewById(R.id.btnRespondToJob);
        btnRespondToJob.setOnClickListener(this);
        textBtnResponse = (TextView)findViewById(R.id.textBtnResponse);

        textTitle = (TextView) findViewById(R.id.textTitle);
        textCost = (TextView) findViewById(R.id.textCost);
        textAddres = (TextView) findViewById(R.id.textAddres);
        textDescription = (TextView) findViewById(R.id.textDescription);
        textDeadLineDate = (TextView) findViewById(R.id.textDeadLineDate);

        Intent intent = getIntent();

        jobID = (int) intent.getLongExtra(JOB_ID, 1);

        textBtnResponse.setText(getString(R.string.add_response));
        textBtnResponse.setTextColor(getResources().getColor(R.color.white));
        btnRespondToJob.setEnabled(false);
        new ReceiveDataJob(this).execute();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnRespondToJob:
                new RecordResponse(this).execute();
                break;
        }
    }


    private class ReceiveDataJob extends AsyncTask<Void, Void, String> {

        private Context context;

        public ReceiveDataJob(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            GetJobApplication myApplication = (GetJobApplication) getApplication();
            ManagerSaveDate managerSaveDate = new ManagerSaveDate();

            try {

                return myApplication.getAPI().getJobData(
                        managerSaveDate.loadAuthorizationData(context), jobID);

            } catch (IOException e) {

                return ERROR_CONNECT;
            }

        }

        @Override
        protected void onPostExecute(String responseString) {
            super.onPostExecute(responseString);

            Log.d(LOG_TAG, "responseString get job = " + responseString);

            if (responseString.equals(ERROR_CONNECT)) {

                Toast.makeText(getBaseContext(), getString(R.string.error_connection),
                        Toast.LENGTH_LONG).show();

                mpDialog.dismiss();

            } else {

                JSONObject dataJsonObj = null;

                try {
                    dataJsonObj = new JSONObject(responseString);
                    JSONArray jobArray = dataJsonObj.getJSONArray(LIST_JOB);

                    String responsePerformer = "";

                    for (int i = 0; i < jobArray.length(); i++) {
                        JSONObject jobObject = jobArray.getJSONObject(i);

                        textTitle.setText(jobObject.getString(TITLE));
                        textCost.setText(jobObject.getString(COST));
                        textAddres.setText(jobObject.getString(ADDRES));
                        textDescription.setText(jobObject.getString(DESCRIPTION));
                        textDeadLineDate.setText(Utils.convertDateToCard(
                                context, jobObject.getString(DEAD_LINE_TIME)));
                        responsePerformer = jobObject.getString(RESPONSE);
                    }

                    if (responsePerformer.equals(ERROR_RESPONSE)) {
                        textBtnResponse.setText(getString(R.string.response_already_exists));
                        textBtnResponse.setTextColor(getResources().getColor(R.color.text_disabled));
                        btnRespondToJob.setEnabled(false);

                    } else {
                        textBtnResponse.setText(getString(R.string.add_response));
                        btnRespondToJob.setEnabled(true);
                    }

                    downloadArea.setVisibility(View.VISIBLE);

                    mpDialog.dismiss();

                } catch (JSONException e) {

                    mpDialog.dismiss();
                }


            }
        }
    }

    private class RecordResponse extends AsyncTask<Void, Void, String> {

        private Context context;

        public RecordResponse(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            GetJobApplication myApplication = (GetJobApplication) getApplication();
            ManagerSaveDate managerSaveDate = new ManagerSaveDate();

            try {

                return myApplication.getAPI().addResponseJob(
                        managerSaveDate.loadAuthorizationData(context), jobID);

            } catch (IOException e) {

                return ERROR_CONNECT;
            }

        }

        @Override
        protected void onPostExecute(String responseString) {
            super.onPostExecute(responseString);

            Log.d(LOG_TAG, "responseString get job = " + responseString);

            if (responseString.equals(TRUE_TEXT)) {

                textBtnResponse.setText(getString(R.string.response_already_exists));
                textBtnResponse.setTextColor(getResources().getColor(R.color.text_disabled));
                btnRespondToJob.setEnabled(false);

                Toast.makeText(getBaseContext(), getString(R.string.added_feedback),
                        Toast.LENGTH_LONG).show();

            } else if (responseString.equals(ERROR_RESPONSE)) {

                Toast.makeText(getBaseContext(), getString(R.string.response_already_exists),
                        Toast.LENGTH_LONG).show();

            } else if (responseString.equals(ERROR_CONNECT)) {

                Toast.makeText(getBaseContext(), getString(R.string.error_connection),
                        Toast.LENGTH_LONG).show();
            } else {

                Toast.makeText(getBaseContext(), getString(R.string.transmission_fails),
                        Toast.LENGTH_LONG).show();
            }

            mpDialog.dismiss();
        }
    }
}
