package by.ef.alex.exchangefreelancing.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import by.ef.alex.exchangefreelancing.R;

/**
 * Отображает точку на карте
 */
public class AddMarkerActivity extends FragmentActivity {

    private final String COORDINATE_X = "coordinateX";
    private final String COORDINATE_Y = "coordinateY";

    private GoogleMap map;
    private Double coordinateX = null;
    private Double coordinateY = null;

    public static void start(Context context, Activity activity) {

        Intent intent = new Intent(context, AddMarkerActivity.class);
        activity.startActivityForResult(intent, 1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(new ColorDrawable(0));

        setUpMapIfNeeded();
        map.setOnMapClickListener(new MapClickListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_add_marker, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.item1:

                if (coordinateX != null && coordinateY != null) {
                    Intent intent = new Intent();
                    intent.putExtra(COORDINATE_X, coordinateX);
                    intent.putExtra(COORDINATE_Y, coordinateY);

                    setResult(RESULT_OK, intent);
                    finish();
                } else {

                    Toast.makeText(getBaseContext(), getString(R.string.point_not_selected),
                            Toast.LENGTH_LONG).show();
                }

                break;
        }

        return true;
    }

    /**
     * Устанавливает маркер на карту
     */
    private void setupMarker(LatLng latLng) {
        map.clear();

        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        markerOptions.title(latLng.toString());
        markerOptions.snippet("" + latLng.latitude + "-" + latLng.longitude);
        map.addMarker(markerOptions);

        coordinateX = latLng.latitude;
        coordinateY = latLng.longitude;
    }

    //инициализация карты и приближение к твоей точке
    private void setUpMapIfNeeded() {
        if (map == null) {
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap))
                    .getMap();
            map.setMyLocationEnabled(true);
        }
    }

    /**
     * Получение координат и установка маркера при нажатии на карту
     */
    private class MapClickListener implements OnMapClickListener {

        @Override
        public void onMapClick(LatLng latLng) {
            setupMarker(latLng);
        }
    }
}
