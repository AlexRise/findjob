package by.ef.alex.exchangefreelancing.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

import by.ef.alex.exchangefreelancing.R;

/**
 * класс реализует DialogFragment который позволяет выбрать час и минуту (время)
 */

public class TimeDialogFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private Boolean timeCheck = false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onStart() {
        super.onStart();

        Button nButton = ((AlertDialog) getDialog())
                .getButton(DialogInterface.BUTTON_POSITIVE);
        nButton.setText(getResources().getString(R.string.ready));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        String textHour = "";
        String textMinute = "";

        if (hourOfDay < 10) {
            textHour = "0" + hourOfDay;
        } else {
            textHour = Integer.toString(hourOfDay);
        }
        if (minute < 10) {
            textMinute = "0" + minute;
        } else {
            textMinute = Integer.toString(minute);
        }

        EditText addDeadLineTime  = (EditText) getActivity().findViewById(R.id.addDeadLineTime);

        addDeadLineTime.setText(textHour + ":" + textMinute);

        timeCheck = true;

    }

    public Boolean getTimeCheck() {
        return timeCheck;
    }

    public void setTimeCheck(Boolean dateCheck) {
        this.timeCheck = dateCheck;
    }

}


