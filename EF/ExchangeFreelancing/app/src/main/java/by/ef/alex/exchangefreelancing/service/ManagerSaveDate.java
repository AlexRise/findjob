package by.ef.alex.exchangefreelancing.service;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import by.ef.alex.exchangefreelancing.entity.UserRequisitesObject;

public class ManagerSaveDate {

    private final String LOGIN_TEXT = "login";
    private final String PASSWORD_TEXT = "password";
    private final String USER_TYPE_TEXT = "userType";
    private final String NAME_PREFERENCES = "Authorization";


    SharedPreferences sPref;

    public void saveAuthorizationData(Context context, UserRequisitesObject userRequisites) {
        sPref = context.getSharedPreferences(NAME_PREFERENCES,context.MODE_PRIVATE);
        Editor ed = sPref.edit();
        ed.putString(LOGIN_TEXT, userRequisites.getLogin());
        ed.putString(PASSWORD_TEXT, userRequisites.getPassword());
        ed.putString(USER_TYPE_TEXT, userRequisites.getUserType());
        ed.commit();
    }

    public UserRequisitesObject loadAuthorizationData(Context context) {
        sPref = context.getSharedPreferences(NAME_PREFERENCES,context.MODE_PRIVATE);
        UserRequisitesObject userRequisites = new UserRequisitesObject(
                sPref.getString(LOGIN_TEXT, ""),
                sPref.getString(PASSWORD_TEXT, ""),
                sPref.getString(USER_TYPE_TEXT, ""));

       return userRequisites;
    }


}
