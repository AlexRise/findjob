package by.ef.alex.exchangefreelancing.utils;

import android.content.Context;
import android.text.TextUtils;
import java.util.Calendar;

import by.ef.alex.exchangefreelancing.R;

/**
 * класс реализующий Util методы
 */

public class Utils {

    /**
     * метод реализует проверку неопреденного количества строк на пустоту
     */

    public static boolean hasEmpty(String... values) {
        for (String text : values) {
            if (TextUtils.isEmpty(text)) {
                return true;
            }
        }
        return false;
    }

    /**
     * метод реализует конвертацию даты из "yyyy.mm.dd-hh.mm" формата в "dd Абривиатура месяца"
     * либо dd.mm.yyyy если дата создания задания была в прошлом году
     */

    //TODO: доработать метод, добавить проверку на валидность получаемого параметра
    public static String convertDateToList(Context context, String dateString) {

        String monthName[] = context.getResources().getStringArray(R.array.monthName);

        String newFormatDate;

        Calendar today = Calendar.getInstance();

        String currentDate = String.format("%04d",  today.get(Calendar.YEAR));

        String year = dateString.substring(0, 4);
        String month = dateString.substring(5, 7);
        String number = dateString.substring(8, 10);

        if (currentDate.substring(0, 4).equals(year)) {

            if(Integer.valueOf(number) <10){ number = number.substring(1, 2); }

            newFormatDate = number + " " + monthName[Integer.valueOf(month)-1];

        } else {

            newFormatDate = number + "." + month + "." + year;
        }

        return newFormatDate;
    }

    /**
     * метод реализует конвертацию даты из "yyyy.mm.dd-hh.mm" формата в "dd Абривиатура месяца - hh.mm"
     * либо dd.mm.yyyy - hh.mm если дата создания задания была в прошлом году
     */

    public static String convertDateToCard (Context context, String dateString) {

        String monthName[] = context.getResources().getStringArray(R.array.monthName);

        String newFormatDate;

        Calendar today = Calendar.getInstance();

        String currentDate = String.format("%04d",  today.get(Calendar.YEAR));

        String year = dateString.substring(0, 4);
        String month = dateString.substring(5, 7);
        String number = dateString.substring(8, 10);
        String time = dateString.substring(11, 16);

        if (currentDate.substring(0, 4).equals(year)) {

            if(Integer.valueOf(number) <10){ number = number.substring(1, 2); }

            newFormatDate = number + " " + monthName[Integer.valueOf(month)-1] + " - " + time;

        } else {

            newFormatDate = number + "." + month + "." + year + " - " + time ;
        }

        return newFormatDate;
    }

}
