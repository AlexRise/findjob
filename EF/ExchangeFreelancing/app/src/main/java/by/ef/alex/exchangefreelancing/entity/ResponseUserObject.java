package by.ef.alex.exchangefreelancing.entity;


/**
 * класс реализует объект для формирования конкретного пункта списка откликов от исполнителей
 */

public class ResponseUserObject {

    private int id;
    private String userName;
    private String userPhone;

    public ResponseUserObject(int id, String userName, String userPhone) {

        this.id = id;
        this.userName = userName;
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public int getId() {
        return id;
    }

    public String getUserPhone() {
        return userPhone;
    }


}
