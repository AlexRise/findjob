package by.ef.alex.exchangefreelancing.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.fragment.InformationAboutJobFragment;
import by.ef.alex.exchangefreelancing.fragment.ListResponsesToJobFragment;

/**
 * реализует адаптер для viewpager'a
 */

public class CardJobPagerAdapter extends FragmentPagerAdapter {

    private final String JOB_ID_TEXT = "jobID";
    public static String LOG_TAG = "myLog";

    private Fragment frag [] = new Fragment[2];

    private String title [] ;

    public CardJobPagerAdapter(Context context,FragmentManager fm, int jobID){
        super(fm);

        title = context.getResources().getStringArray(R.array.page_title);

        Bundle bundle = new Bundle();
        bundle.putInt(JOB_ID_TEXT, jobID);

        frag[0] = new InformationAboutJobFragment();
        frag[0].setArguments(bundle);

        frag[1] = new ListResponsesToJobFragment();
        frag[1].setArguments(bundle);
    }

    public CharSequence getPageTitle(int position){

        return title[position];

    }

    @Override
    public Fragment getItem(int position) {
        return frag[position];
    }

    @Override
    public int getCount() {
        return frag.length;
    }

}
