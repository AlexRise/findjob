package by.ef.alex.exchangefreelancing.fragment;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import by.ef.alex.exchangefreelancing.GetJobApplication;
import by.ef.alex.exchangefreelancing.service.ManagerProgresDialog;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.service.ManagerSaveDate;
import by.ef.alex.exchangefreelancing.utils.Utils;

/**
 * класс реализует первую страницу карточки задания для заказчика, на которой отображается вся
 * информация по данному заданию
 */

public class InformationAboutJobFragment extends Fragment {

    private static final String TEXT_JOB_ID = "jobID";

    private final String LIST_JOB = "listJob";
    private final String TITLE = "title";
    private final String COST = "cost";
    private final String ADDRES = "addres";
    private final String DESCRIPTION = "description";
    private final String DEAD_LINE_TIME = "deadLineTime";

    private final String ERROR_CONNECT = "error_connect";

    public static String LOG_TAG = "myLog";

    private TextView textTitle;
    private TextView textCost;
    private TextView textAddres;
    private TextView textDescription;
    private TextView textDeadLineDate;

    private ManagerProgresDialog mpDialog;
    private LinearLayout downloadArea;

    private int jobID;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_information_about_job, null);

        downloadArea = (LinearLayout)view.findViewById(R.id.downloadArea);
        downloadArea.setVisibility(View.GONE);

        textTitle = (TextView) view.findViewById(R.id.textTitle);
        textCost = (TextView) view.findViewById(R.id.textCost);
        textAddres = (TextView) view.findViewById(R.id.textAddres);
        textDescription = (TextView) view.findViewById(R.id.textDescription);


        textDeadLineDate = (TextView) view.findViewById(R.id.textDeadLineDate);
        return view;
    }

    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        jobID = bundle.getInt(TEXT_JOB_ID);

        new WorkingDataServer(getActivity()).execute();
    }

    private class WorkingDataServer extends AsyncTask<Void, Void, String> {

        private Context context;


        public WorkingDataServer(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            GetJobApplication myApplication = (GetJobApplication) getActivity().getApplication();
            ManagerSaveDate managerSaveDate = new ManagerSaveDate();

            try {

                return myApplication.getAPI().getJobData(
                        managerSaveDate.loadAuthorizationData(context), jobID);

            } catch (IOException e) {

                return ERROR_CONNECT;
            }
        }

        @Override
        protected void onPostExecute(String responseString) {
            super.onPostExecute(responseString);

            Log.d(LOG_TAG, "responseString get job = " + responseString);

            if (responseString.equals(ERROR_CONNECT)) {

                Toast.makeText(getActivity(), getString(R.string.error_connection),
                        Toast.LENGTH_LONG).show();

                mpDialog.dismiss();

            } else {

                JSONObject dataJsonObj = null;

                try {
                    dataJsonObj = new JSONObject(responseString);
                    JSONArray jobArray = dataJsonObj.getJSONArray(LIST_JOB);

                    for (int i = 0; i < jobArray.length(); i++) {
                        JSONObject jobObject = jobArray.getJSONObject(i);

                        textTitle.setText(jobObject.getString(TITLE));
                        textCost.setText(jobObject.getString(COST));
                        textAddres.setText(jobObject.getString(ADDRES));
                        textDescription.setText(jobObject.getString(DESCRIPTION));
                        textDeadLineDate.setText(Utils.convertDateToCard(
                                context, jobObject.getString(DEAD_LINE_TIME)));
                    }

                    downloadArea.setVisibility(View.VISIBLE);

                    mpDialog.dismiss();

                } catch (JSONException e) {

                    mpDialog.dismiss();
                }

            }
        }
    }
}
