package by.ef.alex.exchangefreelancing.activity;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import by.ef.alex.exchangefreelancing.GetJobApplication;
import by.ef.alex.exchangefreelancing.entity.JobItem;
import by.ef.alex.exchangefreelancing.service.ManagerProgresDialog;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.adapter.JobListAdapter;
import by.ef.alex.exchangefreelancing.service.ManagerSaveDate;


/*

личный кабинет заказчика, является окном списка в котором отображаются все задания оставленные
этим пользователем.
Имеет action bar с кнопкой добавления новых заданий

 */

public class ListJobCustomerActivity extends ListActivity implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private final String LIST_JOB = "listJob";
    private final String TITLE = "title";
    private final String COST = "cost";
    private final String ADDRES = "addres";
    private final String DESCRIPTION = "description";
    private final String DEAD_LINE_TIME = "deadLineTime";
    private final String CREATION_DATE_DOCUMENT = "creationDateDocument";

    private final String ERROR_CONNECT = "error_connect";
    private static final int NEW_JOB = 1;

    public static String LOG_TAG = "myLog";

    private FloatingActionButton fab;
    private TextView textListEmpty;

    private ManagerProgresDialog mpDialog;
    private JobListAdapter jobListAdapter;

    SwipeRefreshLayout mSwipeRefreshLayout;

    private ArrayList<JobItem> jobList = new ArrayList<JobItem>();

    public static void start(Context context) {

        Intent intent = new Intent(context, ListJobCustomerActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_job_customer);

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(new ColorDrawable(0));

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        textListEmpty = (TextView) findViewById(R.id.textListEmpty);

        textListEmpty.setVisibility(View.GONE);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.orange, R.color.blue, R.color.red);

        registerForContextMenu(this.getListView());

        new WorkingDataServer(this).execute();
    }


    @Override
    public void onRefresh() {

        mSwipeRefreshLayout.setRefreshing(true);

        new WorkingDataServer(this).execute();

        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fab:
                AddJobActivity.start(this);
                break;
        }
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        CardJobToCustomerActivity.start(this, id);
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(0, NEW_JOB, 0, getResources().getString(R.string.add_job));
    }

    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case NEW_JOB:

                AddJobActivity.start(this);

                break;
        }

        return super.onContextItemSelected(item);
    }

    private class WorkingDataServer extends AsyncTask<Void, Void, String> {

        private Context context;

        public WorkingDataServer(Context _context) {

            context = _context;
        }

        protected void onPreExecute() {
            super.onPreExecute();

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            GetJobApplication myApplication = (GetJobApplication) getApplication();
            ManagerSaveDate managerSaveDate = new ManagerSaveDate();

            try {

                return  myApplication.getAPI().getAllMyJob(managerSaveDate.loadAuthorizationData(context));

            } catch (IOException e) {

                return ERROR_CONNECT;
            }
        }

        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            Log.d(LOG_TAG, "responseString = " + strJson);

            if (strJson.equals(ERROR_CONNECT)) {

                Toast.makeText(getBaseContext(), getString(R.string.error_connection),
                        Toast.LENGTH_LONG).show();

            } else {

                jobList.clear();
                jobListAdapter = new JobListAdapter(context, jobList);

                try {

                    JSONObject dataJsonObj = null;

                    dataJsonObj = new JSONObject(strJson);

                    JSONArray jobArray = dataJsonObj.getJSONArray(LIST_JOB);

                    for (int i = 0; i < jobArray.length(); i++) {
                        JSONObject jobObject = jobArray.getJSONObject(i);

                        jobList.add(new JobItem(
                                Integer.valueOf(jobObject.getString("_id")),
                                jobObject.getString(TITLE),
                                jobObject.getString(COST),
                                jobObject.getString(DESCRIPTION),
                                jobObject.getString(DEAD_LINE_TIME),
                                jobObject.getString(ADDRES),
                                jobObject.getString(CREATION_DATE_DOCUMENT)));
                    }

                    jobListAdapter.notifyDataSetChanged();

                    setListAdapter(jobListAdapter);

                    if (jobList.size() == 0) {

                        textListEmpty.setVisibility(View.VISIBLE);

                    } else {

                        textListEmpty.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {

                    jobListAdapter.notifyDataSetChanged();

                    setListAdapter(jobListAdapter);
                }
            }

            mpDialog.dismiss();
        }
    }
}
