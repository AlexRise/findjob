package by.ef.alex.exchangefreelancing.activity;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import by.ef.alex.exchangefreelancing.GetJobApplication;
import by.ef.alex.exchangefreelancing.entity.JobItem;
import by.ef.alex.exchangefreelancing.service.ManagerProgresDialog;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.adapter.JobListAdapter;
import by.ef.alex.exchangefreelancing.service.ManagerSaveDate;


/**
 * личный кабинет ИСПОЛНИТЕЛЯ, является окном списка в котором отображаются все задания всех
 * заказчиков
 */

public class ListJobPerformerActivity extends ListActivity implements
        SwipeRefreshLayout.OnRefreshListener,View.OnClickListener {

    private final String LIST_JOB = "listJob";
    private final String TITLE = "title";
    private final String COST = "cost";
    private final String ADDRES = "addres";
    private final String DESCRIPTION = "description";
    private final String DEAD_LINE_TIME = "deadLineTime";
    private final String CREATION_DATE_DOCUMENT = "creationDateDocument";

    public static String LOG_TAG = "myLog";
    private final String ERROR_CONNECT = "error_connect";

    private TextView textListEmpty;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ManagerProgresDialog mpDialog;
    private JobListAdapter jobListAdapter;

    private LinearLayout layoutButtonToMap;

    private ArrayList<JobItem> jobList = new ArrayList<JobItem>();

    public static void start(Context context) {

        Intent intent = new Intent(context, ListJobPerformerActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_job_performer);

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(new ColorDrawable(0));

        layoutButtonToMap = (LinearLayout)findViewById(R.id.layoutButtonToMap);
        layoutButtonToMap.setOnClickListener(this);

        textListEmpty = (TextView) findViewById(R.id.textListEmpty);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.orange, R.color.blue, R.color.red);

        new WorkingDataServer(this).execute();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.layoutButtonToMap : ShowMarkerActivity.start(this); break;
        }
    }


    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        CardJobToPerformerActivity.start(this, id);
    }

    @Override
    public void onRefresh() {

        mSwipeRefreshLayout.setRefreshing(true);

        new WorkingDataServer(this).execute();

        mSwipeRefreshLayout.setRefreshing(false);
    }

    private class WorkingDataServer extends AsyncTask<Void, Void, String> {

        private Context context;

        public WorkingDataServer(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            GetJobApplication myApplication = (GetJobApplication) getApplication();
            ManagerSaveDate managerSaveDate = new ManagerSaveDate();

            try {

                return  myApplication.getAPI().getAllJob(managerSaveDate.loadAuthorizationData(context));

            } catch (IOException e) {

                return ERROR_CONNECT;
            }
        }

        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            Log.d(LOG_TAG, "responseString = " + strJson);

            if (strJson.equals(ERROR_CONNECT)) {

                Toast.makeText(getBaseContext(), getString(R.string.error_connection),
                        Toast.LENGTH_LONG).show();

            } else {

                try {

                    jobList.clear();
                    jobListAdapter = new JobListAdapter(context, jobList);

                    JSONObject dataJsonObj = null;

                    dataJsonObj = new JSONObject(strJson);
                    JSONArray jobArray = dataJsonObj.getJSONArray(LIST_JOB);

                    for (int i = 0; i < jobArray.length(); i++) {
                        JSONObject jobObject = jobArray.getJSONObject(i);

                        jobList.add(new JobItem(
                                Integer.valueOf(jobObject.getString("_id")),
                                jobObject.getString(TITLE),
                                jobObject.getString(COST),
                                jobObject.getString(DESCRIPTION),
                                jobObject.getString(DEAD_LINE_TIME),
                                jobObject.getString(ADDRES),
                                jobObject.getString(CREATION_DATE_DOCUMENT)
                        ));
                    }

                    jobListAdapter.notifyDataSetChanged();
                    setListAdapter(jobListAdapter);

                    if (jobList.size() == 0) {

                        textListEmpty.setVisibility(View.VISIBLE);
                    } else {

                        textListEmpty.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {

                    jobListAdapter.notifyDataSetChanged();
                    setListAdapter(jobListAdapter);
                }
            }

            mpDialog.dismiss();
        }
    }
}
