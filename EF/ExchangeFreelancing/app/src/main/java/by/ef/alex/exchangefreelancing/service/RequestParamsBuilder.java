package by.ef.alex.exchangefreelancing.service;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * класс Builder, формирует объект List<NameValuePair>
  */

public class RequestParamsBuilder {

    private List<NameValuePair> listNameValuePair;

    public RequestParamsBuilder(){

        listNameValuePair = new ArrayList<NameValuePair>();

    }

    /**
     * добавление дынных и создание объектра BasicNameValuePair с последующим добавлением в ArrayList
     * принимает String String
     * возвращает состояние текущего объекта
     */

    public RequestParamsBuilder add(String name, String value){

        listNameValuePair.add(new BasicNameValuePair(name, value));
        return this;
    }

    /**
     * добавление дынных и создание объектра BasicNameValuePair с последующим добавлением в ArrayList
     * принимает String int
     * возвращает состояние текущего объекта
     */

    public RequestParamsBuilder add(String name, int value){

       listNameValuePair.add(new BasicNameValuePair(name,Integer.toString(value)));
       return this;
    }

    /**
     * добавление дынных и создание объектра BasicNameValuePair с последующим добавлением в ArrayList
     * принимает String Double
     * возвращает состояние текущего объекта
     */

    public RequestParamsBuilder add(String name, Double value){

        listNameValuePair.add(new BasicNameValuePair(name,Double.toString(value)));
        return this;
    }

    /**
     * возвращает сформированный объект типа List<NameValuePair>
      */

    public List<NameValuePair> build(){

        return listNameValuePair;

    }

}
