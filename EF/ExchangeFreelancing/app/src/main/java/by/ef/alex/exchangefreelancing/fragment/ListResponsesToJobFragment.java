package by.ef.alex.exchangefreelancing.fragment;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import by.ef.alex.exchangefreelancing.GetJobApplication;
import by.ef.alex.exchangefreelancing.service.ManagerProgresDialog;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.entity.ResponseUserObject;
import by.ef.alex.exchangefreelancing.adapter.ResponseListAdapter;
import by.ef.alex.exchangefreelancing.service.ManagerSaveDate;

/**
 * класс реализует вторую страницу карточки задания для заказчика, на которой отображаются все
 * отклики для конкретного задания
 */

public class ListResponsesToJobFragment extends ListFragment implements
        SwipeRefreshLayout.OnRefreshListener {

    private final String LIST_RESPONSE = "listResponse";
    private final String ID = "_id";
    private final String USER_NAME = "userName";
    private final String USER_PHONE = "userPhone";

    private static final String TEXT_JOB_ID = "jobID";
    private final String ERROR_CONNECT = "error_connect";
    public static String LOG_TAG = "myLog";

    private TextView textListEmpty;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private ManagerProgresDialog mpDialog;

    private int jobID;

    private ResponseListAdapter responseListAdapter;
    private ArrayList<ResponseUserObject> responseUserList = new ArrayList<ResponseUserObject>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_responses_to_job, null);
        textListEmpty = (TextView) view.findViewById(R.id.textListEmpty);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.orange, R.color.blue, R.color.red);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {

        registerForContextMenu(this.getListView());
        super.onActivityCreated(savedInstanceState);
    }

    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        jobID = bundle.getInt(TEXT_JOB_ID);

        new WorkingDataServer(getActivity()).execute();
    }

    @Override
    public void onRefresh() {

        mSwipeRefreshLayout.setRefreshing(true);
        new WorkingDataServer(getActivity()).execute();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private class WorkingDataServer extends AsyncTask<Void, Void, String> {

        private Context context;

        public WorkingDataServer(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            GetJobApplication myApplication = (GetJobApplication)getActivity().getApplication();
            ManagerSaveDate managerSaveDate = new ManagerSaveDate();

            try {

                return  myApplication.getAPI().getResponseList(
                        managerSaveDate.loadAuthorizationData(context), jobID);

            } catch (IOException e) {

                return ERROR_CONNECT;
            }
        }

        @Override
        protected void onPostExecute(String responseString) {
            super.onPostExecute(responseString);

            Log.d(LOG_TAG, "responseString get job = " + responseString);

            if (responseString.equals(ERROR_CONNECT)) {

                Toast.makeText(getActivity(), getString(R.string.error_connection),
                        Toast.LENGTH_LONG).show();

                mpDialog.dismiss();

            } else {

                try {

                    responseUserList.clear();
                    responseListAdapter = new ResponseListAdapter(context, responseUserList);

                    JSONObject dataJsonObj = null;

                    dataJsonObj = new JSONObject(responseString);
                    JSONArray jobArray = dataJsonObj.getJSONArray(LIST_RESPONSE);

                    for (int i = 0; i < jobArray.length(); i++) {
                        JSONObject jobObject = jobArray.getJSONObject(i);

                        responseUserList.add(new ResponseUserObject(
                                Integer.valueOf(jobObject.getString(ID)),
                                jobObject.getString(USER_NAME),
                                jobObject.getString(USER_PHONE)));
                    }

                    responseListAdapter.notifyDataSetChanged();

                    setListAdapter(responseListAdapter);

                    if (responseUserList.size() == 0) {

                        textListEmpty.setVisibility(View.VISIBLE);

                    } else {

                        textListEmpty.setVisibility(View.GONE);
                    }

                    mpDialog.dismiss();

                } catch (JSONException e) {

                    responseListAdapter.notifyDataSetChanged();

                    setListAdapter(responseListAdapter);

                    mpDialog.dismiss();
                }
            }
        }
    }
}
