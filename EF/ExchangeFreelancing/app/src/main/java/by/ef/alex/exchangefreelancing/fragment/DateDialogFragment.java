package by.ef.alex.exchangefreelancing.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import by.ef.alex.exchangefreelancing.R;

/*

класс реализует DialogFragment который позволяет выбрать день, месяц, год (дату)

 */

public class DateDialogFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private Boolean dateCheck = false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onStart() {
        super.onStart();

        Button nButton = ((AlertDialog) getDialog())
                .getButton(DialogInterface.BUTTON_POSITIVE);
        nButton.setText(getResources().getString(R.string.ready));

    }

    //метод получает выбранную дату, приводит ее к нужному виду, и идобавляет ее как надпись
    // Button btnAddDeadLineDate

    @Override
    public void onDateSet(DatePicker datePicker, int year,
                          int month, int day) {

        String textDay = "";
        String textMonth = "";

        if (day < 10) {
            textDay = "0" + day;
        } else {
            textDay = Integer.toString(day);
        }
        if (month < 10) {
            textMonth = "0" + month;
        } else {
            textMonth = Integer.toString(month);
        }

        EditText addDeadLineDate  = (EditText) getActivity().findViewById(R.id.addDeadLineDate);
        addDeadLineDate.setText(year + "." + textMonth + "." + textDay);

        dateCheck = true;
    }

    public Boolean getDateCheck() {
        return dateCheck;
    }

    public void setDateCheck(Boolean dateCheck) {
        this.dateCheck = dateCheck;
    }

}


