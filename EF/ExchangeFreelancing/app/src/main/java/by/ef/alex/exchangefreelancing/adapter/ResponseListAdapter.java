package by.ef.alex.exchangefreelancing.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.entity.ResponseUserObject;

import static java.lang.Integer.valueOf;

/*

Класс реализует BaseAdapter  списка откликов карточки задания, для заказчика

 */


public class ResponseListAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater lInflater;
    private ArrayList<ResponseUserObject> objects;

    public ResponseListAdapter(Context context, ArrayList<ResponseUserObject> objects) {

        this.context = context;
        this.objects = objects;

        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return valueOf(objects.get(position).getId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = lInflater.inflate(R.layout.item_list_response, parent, false);

            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ResponseUserObject responseUserObject = (ResponseUserObject) getItem(position);

        viewHolder.textUserName.setText(responseUserObject.getUserName());
        viewHolder.textUserPhone.setText(responseUserObject.getUserPhone());

        return convertView;
    }

    //реализация паттерна ViewHolder
    private class ViewHolder {

        private TextView textUserName;
        private TextView textUserPhone;

        public ViewHolder(View convertView) {

            textUserName = (TextView) convertView.findViewById(R.id.textUserName);
            textUserPhone = (TextView) convertView.findViewById(R.id.textUserPhone);
        }
    }
}
