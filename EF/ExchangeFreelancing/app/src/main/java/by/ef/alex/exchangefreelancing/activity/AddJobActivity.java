package by.ef.alex.exchangefreelancing.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;

import by.ef.alex.exchangefreelancing.GetJobApplication;
import by.ef.alex.exchangefreelancing.entity.JobObjectToSend;
import by.ef.alex.exchangefreelancing.service.ManagerProgresDialog;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.fragment.DateDialogFragment;
import by.ef.alex.exchangefreelancing.fragment.TimeDialogFragment;
import by.ef.alex.exchangefreelancing.service.ManagerSaveDate;
import by.ef.alex.exchangefreelancing.utils.Utils;

/**
 * окно добавления новых заданий
 */

public class AddJobActivity extends Activity implements View.OnClickListener {

    private final String TRUE_TEXT = "true";
    private final String EMPTY_TEXT = "";
    private final String COORDINATE_X = "coordinateX";
    private final String COORDINATE_Y = "coordinateY";
    public static String LOG_TAG = "myLog";
    private final String ERROR_CONNECT = "error_connect";

    private DateDialogFragment dateDialog;
    private TimeDialogFragment timeDialog;

    private JobObjectToSend jobObjectToSend;

    private EditText textTitle;
    private EditText textCost;
    private EditText textDescription;
    private EditText textAddres;
    private EditText addDeadLineDate;
    private EditText addDeadLineTime;

    private LinearLayout addMarker;
    private LinearLayout btnBack;

    private LinearLayout btnAddJob;

    private TextView textAddMarker;

    private ManagerProgresDialog mpDialog;

    private Double coordinateX = 0.0;
    private Double coordinateY = 0.0;

    private String creationDateDocument;

    public static void start(Context context) {

        Intent intent = new Intent(context, AddJobActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_job);

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(new ColorDrawable(0));

        textTitle = (EditText) findViewById(R.id.textTitle);
        textCost = (EditText) findViewById(R.id.textCost);
        textDescription = (EditText) findViewById(R.id.textDescription);
        textAddres = (EditText) findViewById(R.id.textAddres);

        addDeadLineDate = (EditText) findViewById(R.id.addDeadLineDate);
        addDeadLineDate.setFocusable(false);
        addDeadLineDate.setLongClickable(false);
        addDeadLineDate.setCursorVisible(false);
        addDeadLineDate.setOnClickListener(this);

        addDeadLineTime = (EditText) findViewById(R.id.addDeadLineTime);
        addDeadLineTime.setFocusable(false);
        addDeadLineTime.setLongClickable(false);
        addDeadLineTime.setCursorVisible(false);
        addDeadLineTime.setOnClickListener(this);

        textAddMarker = (TextView) findViewById(R.id.textAddMarker);

        addMarker = (LinearLayout) findViewById(R.id.addMarker);
        addMarker.setOnClickListener(this);

        btnAddJob = (LinearLayout) findViewById(R.id.btnAddJob);
        btnAddJob.setOnClickListener(this);

        btnBack = (LinearLayout) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);


        dateDialog = new DateDialogFragment();
        timeDialog = new TimeDialogFragment();

        Calendar today = Calendar.getInstance();

        creationDateDocument = String.format("%04d.%02d.%02d-%02d:%02d",
                today.get(Calendar.YEAR),
                today.get(Calendar.MONTH) + 1,
                today.get(Calendar.DAY_OF_MONTH),
                today.get(Calendar.HOUR_OF_DAY),
                today.get(Calendar.MINUTE));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.addMarker:
                AddMarkerActivity.start(this, this);
                break;

            case R.id.addDeadLineDate:
                dateDialog.show(getFragmentManager(), "dateDialog");
                break;

            case R.id.addDeadLineTime:
                timeDialog.show(getFragmentManager(), "timeDialog");
                break;

            case R.id.btnAddJob:
                addJob();
                break;

            case R.id.btnBack:
                finish();
                break;
        }
    }

    private void addJob() {

        boolean checkFlag = true;

        if (!dateDialog.getDateCheck()) {
            checkFlag = false;
        }

        if (!timeDialog.getTimeCheck()) {
            checkFlag = false;
        }

        if (Utils.hasEmpty(textTitle.getText().toString(), textCost.getText().toString(),
                textDescription.getText().toString(), textAddres.getText().toString())) {
            checkFlag = false;
        }

        if (checkFlag) {

            String fullDate = addDeadLineDate.getText().toString() + "-"
                    + addDeadLineTime.getText().toString();
            jobObjectToSend = new JobObjectToSend();

            jobObjectToSend.setTitle(textTitle.getText().toString());
            jobObjectToSend.setCost(textCost.getText().toString());
            jobObjectToSend.setDeadLineDate(fullDate);
            jobObjectToSend.setDescription(textDescription.getText().toString());
            jobObjectToSend.setCoordinateX(coordinateX);
            jobObjectToSend.setCoordinateY(coordinateY);
            jobObjectToSend.setAddres(textAddres.getText().toString());
            jobObjectToSend.setCreationDateDocument(creationDateDocument);

            new LoadJob(this).execute();
        } else {
            Toast.makeText(getBaseContext(), getString(R.string.fill_all_fields), Toast.LENGTH_SHORT).show();
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        coordinateX = data.getDoubleExtra(COORDINATE_X, 0);
        coordinateY = data.getDoubleExtra(COORDINATE_Y, 0);
        textAddMarker.setText(getString(R.string.point_added));
        textAddMarker.setTextColor(getResources().getColor(R.color.greentheme_color));
    }

    class LoadJob extends AsyncTask<String, String, String> {

        private Context context;

        public LoadJob(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(String... params) {

            GetJobApplication myApplication = (GetJobApplication) getApplication();
            ManagerSaveDate managerSaveDate = new ManagerSaveDate();

            try {

                return myApplication.getAPI().addJob(managerSaveDate.loadAuthorizationData(context),
                        jobObjectToSend);

            } catch (IOException e) {

                return ERROR_CONNECT;
            }

        }

        protected void onPostExecute(String responseString) {
            super.onPostExecute(responseString);

            Log.d(LOG_TAG, "responseString11 = " + responseString);

            if (responseString.equals(TRUE_TEXT)) {

                textTitle.setText(EMPTY_TEXT);
                textCost.setText(EMPTY_TEXT);
                textDescription.setText(EMPTY_TEXT);
                textAddres.setText(EMPTY_TEXT);
                textAddMarker.setText(getString(R.string.add_marker));
                textAddMarker.setTextColor(getResources().getColor(R.color.text_list));
                addDeadLineDate.setText(EMPTY_TEXT);
                addDeadLineTime.setText(EMPTY_TEXT);
                dateDialog.setDateCheck(false);
                timeDialog.setTimeCheck(false);


                Toast.makeText(getBaseContext(), getString(R.string.entries_added),
                        Toast.LENGTH_LONG).show();

            } else if (responseString.equals(ERROR_CONNECT)) {

                Toast.makeText(getBaseContext(), getString(R.string.error_connection),
                        Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(getBaseContext(), getString(R.string.transmission_fails),
                        Toast.LENGTH_LONG).show();
            }

            mpDialog.dismiss();
        }
    }
}
