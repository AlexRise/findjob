package by.ef.alex.exchangefreelancing.entity;

/**
 * класс реализует объект для формирования карточки конкретного задания
 */

public class JobObjectToSend {

    private String title;
    private String description;
    private String deadLineDate;
    private String cost;
    private Double coordinateX;
    private Double coordinateY;
    private String addres;
    private String creationDateDocument;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDeadLineDate() {
        return deadLineDate;
    }

    public String getCost() {
        return cost;
    }

    public Double getCoordinateX() {
        return coordinateX;
    }

    public Double getCoordinateY() {
        return coordinateY;
    }

    public String getAddres() {
        return addres;
    }

    public String getCreationDateDocument() {
        return creationDateDocument;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDeadLineDate(String deadLineDate) {
        this.deadLineDate = deadLineDate;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setCoordinateY(Double coordinateY) {
        this.coordinateY = coordinateY;
    }

    public void setCoordinateX(Double coordinateX) {
        this.coordinateX = coordinateX;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public void setCreationDateDocument(String creationDateDocument) {
        this.creationDateDocument = creationDateDocument;
    }

}


