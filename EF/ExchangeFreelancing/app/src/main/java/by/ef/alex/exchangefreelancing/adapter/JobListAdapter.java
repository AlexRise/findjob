package by.ef.alex.exchangefreelancing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import by.ef.alex.exchangefreelancing.entity.JobItem;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.utils.Utils;

import static java.lang.Integer.*;

/*
Класс реализует BaseAdapter  списка заданий для личного кабинета пользователей
*/
public class JobListAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater lInflater;
    private ArrayList<JobItem> objects;

//конструктор принимает параметры(Context и _obj(список объектов которые хранят данные для
// конкретного пункта списка)) для на основе которых будет создаваться список

    public JobListAdapter(Context context, ArrayList<JobItem> objects) {

        this.context = context;
        this.objects = objects;

        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }


    @Override
    public long getItemId(int position) {
        return valueOf(objects.get(position).getId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = lInflater.inflate(R.layout.item_list_job, parent, false);

            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        JobItem jobObject = (JobItem) getItem(position);

        viewHolder.textTitle.setText(jobObject.getTitle());
        viewHolder.textCost.setText(jobObject.getCost());
        viewHolder.textDeadLineDate.setText(jobObject.getAddres());
        viewHolder.textCreationDateDocument.setText(
                Utils.convertDateToList(context, jobObject.getCreationDateDocument()));

        return convertView;
    }

    //реализация паттерна ViewHolder
    private class ViewHolder {

        private TextView textTitle;
        private TextView textCost;
        private TextView textDeadLineDate;
        private TextView textCreationDateDocument;

        public ViewHolder(View convertView) {

            textTitle = (TextView) convertView.findViewById(R.id.textTitle);
            textCost = (TextView) convertView.findViewById(R.id.textCost);
            textDeadLineDate = (TextView) convertView.findViewById(R.id.textDeadLineDate);
            textCreationDateDocument = (TextView) convertView.findViewById(R.id.textCreationDateDocument);
        }
    }
}
