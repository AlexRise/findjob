package by.ef.alex.exchangefreelancing.network;

import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.util.List;

/*

класс реализует подключение к серверу по http протоколу

 */

public class ApplicationHttpClient {

    HttpClient httpclient;

    //инициализация http клиента
    public ApplicationHttpClient() {

        httpclient = new DefaultHttpClient();

    }

    //метод отправляет данные на сервер по указанному url
    //и возвращает ответ сервера

    public HttpResponse doPostRequest(String urlServer, String urlAPI,
                                      List<NameValuePair> nameValuePairs) throws IOException {


        HttpPost httppost = new HttpPost(urlServer + urlAPI);

        httppost.setEntity(new UrlEncodedFormEntity(
                nameValuePairs, HTTP.UTF_8));

        HttpResponse response = httpclient.execute(httppost);

        return response;
    }

    //метод получает ответ сервера, и преобразовывает его в String для его дальнейшей обработки

    public String doGetRequest(HttpResponse response) throws IOException {

        String responseString = new String();


        HttpEntity responseEntity = response.getEntity();

        if (responseEntity != null) {

            responseString = EntityUtils.toString(responseEntity, HTTP.UTF_8);
        }

        return responseString;

    }

}
