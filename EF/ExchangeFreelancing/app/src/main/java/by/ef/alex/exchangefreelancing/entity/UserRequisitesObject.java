package by.ef.alex.exchangefreelancing.entity;

/**
 * Класс предназначен для хранения регистрационных данных пользователя
 */
public class UserRequisitesObject {

    private String login;
    private String password;
    private String userType;

    public UserRequisitesObject(String login, String password, String userType){

        this.login = login;
        this.password = password;
        this.userType = userType;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getUserType() {
        return userType;
    }
}
