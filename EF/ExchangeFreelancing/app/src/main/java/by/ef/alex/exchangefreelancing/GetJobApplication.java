package by.ef.alex.exchangefreelancing;

import android.app.Application;

import by.ef.alex.exchangefreelancing.entity.UserRequisitesObject;
import by.ef.alex.exchangefreelancing.network.API;

/**
 * Экззмпляр приложения
 */
public class GetJobApplication extends Application {

    private API api;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public API getAPI() {
        return api == null ? api = new API() : api;
    }
}
