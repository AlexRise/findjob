package by.ef.alex.exchangefreelancing.activity;


import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.adapter.CardJobPagerAdapter;
import by.ef.alex.exchangefreelancing.view.SlidingTabLayout;


/**
 * Карточка задания для Заказчика
 */

public class CardJobToCustomerActivity extends FragmentActivity {

    private final String JOB_ID_TEXT = "jobID";
    public static String LOG_TAG = "myLog";

    private int jobID;

    ViewPager pager;
    private SlidingTabLayout mSlidingTabLayout;

    /**
     * метод вызывает инициализацию activity, и передает в нее параметры
     */

    public static void start(Context context, long idJob) {

        Intent intent = new Intent(context, CardJobToCustomerActivity.class);
        intent.putExtra("jobID", idJob);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_job_to_customer);

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(new ColorDrawable(0));

        pager = (ViewPager) findViewById(R.id.pager);

        Intent intent = getIntent();

        jobID = (int) intent.getLongExtra(JOB_ID_TEXT, 1);

        CardJobPagerAdapter cardJobPagerAdapter = new CardJobPagerAdapter(this,
                getSupportFragmentManager(),jobID);

        pager.setAdapter(cardJobPagerAdapter);
        pager.setCurrentItem(0);

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(pager);

    }

}
