package by.ef.alex.exchangefreelancing.service;

import android.app.ProgressDialog;
import android.content.Context;

import by.ef.alex.exchangefreelancing.R;

/**
 * Класс отвечает за работу ProgresDialog в приложении
 */

public class ManagerProgresDialog {

    public static String LOG_TAG = "myLog";
    private Context context;
    private ProgressDialog pDialog;

    /**
     * инициализируем объект ProgressDialog
     */

    public ManagerProgresDialog(Context context) {

        this.context = context;
        pDialog = new ProgressDialog(context);
    }

    /**
     * устанавливаем заданные параметры для ProgressDialog и отображаем его на экране,
     * возвращаем состояние объекта
     */

    public ManagerProgresDialog show() {

        pDialog.setMessage(context.getResources().getString(R.string.data_processing_wait));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        return this;
    }

    /**
     *скрываем ProgressDialog
     */

    public void dismiss() {

        pDialog.dismiss();
    }
}
