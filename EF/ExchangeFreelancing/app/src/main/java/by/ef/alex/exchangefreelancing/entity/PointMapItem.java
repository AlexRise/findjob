package by.ef.alex.exchangefreelancing.entity;

/**
 * класс реализует объект предназначенный для формирования точек на карте
 */

public class PointMapItem {

    private int id;
    private String title;
    private Double coordinateХ;
    private Double coordinateY;

    public PointMapItem(int id, String title, Double coordinateХ, Double coordinateY) {

        this.id = id;
        this.title = title;
        this.coordinateХ = coordinateХ;
        this.coordinateY = coordinateY;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getCoordinateХ() {
        return coordinateХ;
    }

    public void setCoordinateХ(Double coordinateХ) {
        this.coordinateХ = coordinateХ;
    }

    public Double getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(Double coordinateY) {
        this.coordinateY = coordinateY;
    }


}
