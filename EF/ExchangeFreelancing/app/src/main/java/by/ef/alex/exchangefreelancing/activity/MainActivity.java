package by.ef.alex.exchangefreelancing.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import by.ef.alex.exchangefreelancing.GetJobApplication;
import by.ef.alex.exchangefreelancing.service.ManagerProgresDialog;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.entity.UserRequisitesObject;
import by.ef.alex.exchangefreelancing.service.ManagerSaveDate;


/**
 * Класс LAUNCHER
 * Является окном авторизации
 */

public class MainActivity extends Activity implements View.OnClickListener {

    private final String EMPTY_TEXT = "";
    private final String TRUE_TEXT = "true";
    private final String ERROR_2 = "error2";
    private final String ERROR_CONNECT = "error_connect";
    public static String LOG_TAG = "myLog";

    private ManagerProgresDialog mpDialog;

    private ImageView imageLogo;
    private EditText editTextLogin;
    private EditText editTextPassword;
    private Button btnStart;
    private LinearLayout layoutError;
    private LinearLayout layoutColorStatus;
    private TextView textErrorAutorization;
    private Spinner spinnerUserType;

    private ArrayList<String> authorizationData = new ArrayList<String>();
    private String[] arrUserType;

    private String login;
    private String password;
    private String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();
        actionBar.hide();

        arrUserType = getResources().getStringArray(R.array.arrUserType);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item, arrUserType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        imageLogo = (ImageView) findViewById(R.id.imageLogo);

        spinnerUserType = (Spinner) findViewById(R.id.spinnerUserType);
        spinnerUserType.setAdapter(adapter);

        editTextLogin = (EditText) findViewById(R.id.editTextLogin);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        textErrorAutorization = (TextView) findViewById(R.id.textErrorAutorization);

        btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(this);

        layoutColorStatus = (LinearLayout) findViewById(R.id.layoutColorStatus);
        layoutError = (LinearLayout) findViewById(R.id.layoutError);
        layoutError.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnStart:

                layoutError.setVisibility(View.GONE);
                textErrorAutorization.setText(EMPTY_TEXT);
                layoutColorStatus.setBackgroundColor(getResources().getColor(R.color.red));
                authorize();
                break;
        }
    }

    private void authorize() {

        Boolean checkValidity = true;
        authorizationData.clear();

        login = EMPTY_TEXT;
        password = EMPTY_TEXT;
        userType = EMPTY_TEXT;

        if (EMPTY_TEXT.equals(editTextLogin.getText().toString())) {
            checkValidity = false;
        }
        if (EMPTY_TEXT.equals(editTextPassword.getText().toString())) {
            checkValidity = false;
        }

        if (checkValidity) {

            login = editTextLogin.getText().toString();
            password = editTextPassword.getText().toString();
            userType = Long.toString(spinnerUserType.getSelectedItemId());

            Log.d(LOG_TAG, "login = " + login);

            new WorkingDataServer(this).execute();

        } else {

            textErrorAutorization.setText(getString(R.string.error1));
            layoutError.setVisibility(View.VISIBLE);
        }
    }

    private class WorkingDataServer extends AsyncTask<String, String, String> {

        private Context context;

        public WorkingDataServer(Context _context) {

            context = _context;
        }

        protected void onPreExecute() {
            super.onPreExecute();

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(String... params) {

            GetJobApplication myApplication = (GetJobApplication) getApplication();

            try {

                return  myApplication.getAPI().autorize(login, password, userType);

            } catch (IOException e) {

                return ERROR_CONNECT;
            }
        }

        protected void onPostExecute(String responseString) {
            super.onPostExecute(responseString);
            Log.d(LOG_TAG, "responseString = " + responseString);
            if (responseString.equals(TRUE_TEXT)) {

                editTextLogin.setText(EMPTY_TEXT);
                editTextPassword.setText(EMPTY_TEXT);

                ManagerSaveDate managerSaveDate = new ManagerSaveDate();

                managerSaveDate.saveAuthorizationData(context,
                        new UserRequisitesObject(login, password, userType));

                switch ((int) spinnerUserType.getSelectedItemId()) {

                    case 0:
                        ListJobCustomerActivity.start(context);
                        break;

                    case 1:
                        ListJobPerformerActivity.start(context);
                        break;
                }

            } else if (responseString.equals(ERROR_2)) {

                layoutError.setVisibility(View.VISIBLE);
                textErrorAutorization.setText(getString(R.string.error2));

            } else if (responseString.equals(ERROR_CONNECT)) {

                layoutError.setVisibility(View.VISIBLE);
                textErrorAutorization.setText(getString(R.string.error_connection));
            } else {

                layoutError.setVisibility(View.VISIBLE);
                textErrorAutorization.setText(getString(R.string.error4));
            }

            mpDialog.dismiss();
        }
    }


}
