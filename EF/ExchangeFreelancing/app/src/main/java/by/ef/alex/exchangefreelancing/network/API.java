package by.ef.alex.exchangefreelancing.network;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import java.io.IOException;
import java.util.List;

import by.ef.alex.exchangefreelancing.entity.JobObjectToSend;
import by.ef.alex.exchangefreelancing.service.RequestParamsBuilder;
import by.ef.alex.exchangefreelancing.entity.UserRequisitesObject;

/**
*класс реализует методы которые формируют список данных для отправки к конкретным API
*/

public class API {

    private final String URL_SERVER = "http://www.oc-incos.by/testProject/";
    private final String URL_CHECK_LOGIN = "check_login.php";
    private final String URL_ADD_JOB = "add_job.php";
    private final String URL_ALL_GET_JOB = "all_get_job.php";
    private final String URL_ALL_GET_MY_JOB = "all_get_my_job.php";
    private final String URL_GET_JOB = "get_job.php";
    private final String URL_ADD_RESPONSE = "add_response.php";
    private final String URL_GET_RESPONSE_LIST = "get_response_list.php";
    private final String URL_GET_POINT_MAP = "get_point_map.php";

    private final String LOGIN = "login";
    private final String PASSWORD = "password";
    private final String USER_TYPE = "userType";
    private final String JOB_ID = "jobID";
    private final String TITLE = "title";
    private final String COST = "cost";
    private final String ADDRES = "addres";
    private final String DESCRIPTION = "description";
    private final String DEAD_LINE_TIME = "deadLineTime";
    private final String COORDINATE_X = "coordinateX";
    private final String COORDINATE_Y = "coordinateY";
    private final String CREATION_DATE_DOCUMENT = "creationDateDocument";

    private final String ERROR_CONNECT = "error_connect";


    public static String LOG_TAG = "myLog";

    private ApplicationHttpClient httpClient;

    public API() {

        httpClient = new ApplicationHttpClient();
    }

    /**
     *  метод формирует список данных для запроса авторизации пользователя
     */

    public String autorize(String login, String password, String userType) throws IOException {

            List<NameValuePair> pairs = new RequestParamsBuilder()
                    .add(LOGIN, login)
                    .add(PASSWORD, password)
                    .add(USER_TYPE, userType)
                    .build();

            HttpResponse response = httpClient.doPostRequest(URL_SERVER, URL_CHECK_LOGIN,
                    pairs);
            return httpClient.doGetRequest(response);
    }

    /**
     *  метод формирует список данных для отправки запроса для получения списка всех заданий
     */
    public String getAllJob(UserRequisitesObject userRequisites) throws IOException {

        List<NameValuePair> pairs = new RequestParamsBuilder()
                .add(LOGIN, userRequisites.getLogin())
                .add(PASSWORD, userRequisites.getPassword())
                .add(USER_TYPE, userRequisites.getUserType())
                .build();

            HttpResponse response = httpClient.doPostRequest(URL_SERVER, URL_ALL_GET_JOB,
                    pairs);
            return httpClient.doGetRequest(response);
    }

    /**
     * метод формирует список данных для отправки запроса для получения списка всех заданий
     * конкретного заказчика
     */

    public String getAllMyJob(UserRequisitesObject userRequisites) throws IOException {


            List<NameValuePair> pairs = new RequestParamsBuilder()
                    .add(LOGIN, userRequisites.getLogin())
                    .add(PASSWORD, userRequisites.getPassword())
                    .add(USER_TYPE, userRequisites.getUserType())
                    .build();

            HttpResponse response = httpClient.doPostRequest(URL_SERVER, URL_ALL_GET_MY_JOB,
                    pairs);
            return httpClient.doGetRequest(response);
    }

    /**
     * метод формирует список данных для отправки запроса на добавление нового задания
     */

    public String addJob(UserRequisitesObject userRequisites,
                         JobObjectToSend jobObjectToSend) throws IOException {

            List<NameValuePair> pairs = new RequestParamsBuilder()
                    .add(LOGIN, userRequisites.getLogin())
                    .add(PASSWORD, userRequisites.getPassword())
                    .add(USER_TYPE, userRequisites.getUserType())
                    .add(TITLE, jobObjectToSend.getTitle())
                    .add(COST, jobObjectToSend.getCost())
                    .add(ADDRES, jobObjectToSend.getAddres())
                    .add(DESCRIPTION, jobObjectToSend.getDescription())
                    .add(DEAD_LINE_TIME, jobObjectToSend.getDeadLineDate())
                    .add(COORDINATE_X, jobObjectToSend.getCoordinateX())
                    .add(COORDINATE_Y, jobObjectToSend.getCoordinateY())
                    .add(CREATION_DATE_DOCUMENT, jobObjectToSend.getCreationDateDocument())
                    .build();

            HttpResponse response = httpClient.doPostRequest(URL_SERVER, URL_ADD_JOB,
                    pairs);
            return httpClient.doGetRequest(response);
    }

    /**
     * метод формирует список данных для отправки запроса на получение конкретного задания
     */

    public String getJobData(UserRequisitesObject userRequisites, int jobID) throws IOException {

            List<NameValuePair> pairs = new RequestParamsBuilder()
                    .add(LOGIN, userRequisites.getLogin())
                    .add(PASSWORD, userRequisites.getPassword())
                    .add(USER_TYPE, userRequisites.getUserType())
                    .add(JOB_ID, jobID)
                    .build();

            HttpResponse response = httpClient.doPostRequest(URL_SERVER, URL_GET_JOB,
                    pairs);
            return httpClient.doGetRequest(response);
    }

    /**
     *  метод формирует список данных для отправки запроса на добавление нового отклика
     */

    public String addResponseJob(UserRequisitesObject userRequisites, int jobID) throws IOException {

            List<NameValuePair> pairs = new RequestParamsBuilder()
                    .add(LOGIN, userRequisites.getLogin())
                    .add(PASSWORD, userRequisites.getPassword())
                    .add(USER_TYPE, userRequisites.getUserType())
                    .add(JOB_ID, jobID)
                    .build();

            HttpResponse response = httpClient.doPostRequest(URL_SERVER, URL_ADD_RESPONSE,
                    pairs);
            return httpClient.doGetRequest(response);
    }

    /**
     * метод формирует список данных для отправки запроса на получение списка всех откликов для
     * конкретного задания
     */

    public String getResponseList(UserRequisitesObject userRequisites, int jobID)
            throws IOException {

            List<NameValuePair> pairs = new RequestParamsBuilder()
                    .add(LOGIN, userRequisites.getLogin())
                    .add(PASSWORD, userRequisites.getPassword())
                    .add(USER_TYPE, userRequisites.getUserType())
                    .add(JOB_ID, jobID)
                    .build();

            HttpResponse response = httpClient.doPostRequest(URL_SERVER, URL_GET_RESPONSE_LIST,
                    pairs);
            return httpClient.doGetRequest(response);
    }

    public String getPointMap(UserRequisitesObject userRequisites) throws IOException {

            List<NameValuePair> pairs = new RequestParamsBuilder()
                    .add(LOGIN, userRequisites.getLogin())
                    .add(PASSWORD, userRequisites.getPassword())
                    .add(USER_TYPE, userRequisites.getUserType())
                    .build();

            HttpResponse response = httpClient.doPostRequest(URL_SERVER, URL_GET_POINT_MAP,
                    pairs);
            return httpClient.doGetRequest(response);
    }
}
