package by.ef.alex.exchangefreelancing.entity;

/**
 * класс реализует объект для формирования конкретного пункта списка заданий в кабинете
 * пользователей
 */

public class JobItem {

    private int id;
    private String title;
    private String description;
    private String deadLineDate;
    private String cost;
    private String addres;
    private String creationDateDocument;

    public JobItem(int id, String title, String cost, String description,
                   String deadLineDate, String addres, String creationDateDocument ) {

        this.id = id;
        this.title = title;
        this.description = description;
        this.deadLineDate = deadLineDate;
        this.cost = cost;
        this.addres = addres;
        this.creationDateDocument = creationDateDocument;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDeadLineDate() {
        return deadLineDate;
    }

    public String getCost() {
        return cost;
    }

    public String getAddres() {
        return addres;
    }

    public String getCreationDateDocument() {
        return creationDateDocument;
    }

    public void setId(int id) {
        this.id = id;
    }

}
