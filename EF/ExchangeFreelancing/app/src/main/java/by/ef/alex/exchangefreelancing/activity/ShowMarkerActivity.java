package by.ef.alex.exchangefreelancing.activity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import by.ef.alex.exchangefreelancing.GetJobApplication;
import by.ef.alex.exchangefreelancing.service.ManagerProgresDialog;
import by.ef.alex.exchangefreelancing.entity.PointMapItem;
import by.ef.alex.exchangefreelancing.R;
import by.ef.alex.exchangefreelancing.service.ManagerSaveDate;

/**
 * Отображает точки на карте
 */
public class ShowMarkerActivity extends FragmentActivity implements GoogleMap.OnMarkerClickListener {

    private final String LIST_POINT_MAP = "listPointMap";
    private final String ID = "_id";
    private final String TITLE = "title";
    private final String COORDINATE_X = "coordinateX";
    private final String COORDINATE_Y = "coordinateY";

    private final String ERROR_CONNECT = "error_connect";
    public static String LOG_TAG = "myLog";

    private GoogleMap map;
    private ManagerProgresDialog mpDialog;

    private Map<Marker, Integer> pointMap = new HashMap<Marker, Integer>();

    private ArrayList<PointMapItem> pointMapList = new ArrayList<PointMapItem>();

    private Boolean stateMarker;
    private int idJob = 1;

    public static void start(Context context) {
        Intent intent = new Intent(context, ShowMarkerActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(new ColorDrawable(0));

        stateMarker = true;

        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap)).getMap();
        map.setOnMarkerClickListener(this);

        new WorkingDataServer(this).execute();

    }

    public boolean onCreateOptionsMenu(Menu menu) {

        if (!stateMarker) {
            getMenuInflater().inflate(R.menu.menu_show_marker, menu);
        } else {

            menu.removeItem(2);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.item1:

                if (!stateMarker) {

                    CardJobToPerformerActivity.start(this, idJob);

                }
                break;
        }

        return true;
    }

    /**
     * Устанавливает маркеры на карту
     */

    private void setMarker() {

        for (PointMapItem point : pointMapList) {

            pointMap.put(map.addMarker(new MarkerOptions()
                            .position(new LatLng(point.getCoordinateХ(), point.getCoordinateY()))
                            .title("" + point.getId())
                            .snippet(point.getTitle())),
                    point.getId());
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        idJob = pointMap.get(marker);

        if (stateMarker) {
            stateMarker = false;
        }

        invalidateOptionsMenu();

        return false;
    }


    private class WorkingDataServer extends AsyncTask<Void, Void, String> {

        private Context context;

        public WorkingDataServer(Context context) {

            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();

            mpDialog = new ManagerProgresDialog(context).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            GetJobApplication myApplication = (GetJobApplication) getApplication();
            ManagerSaveDate managerSaveDate = new ManagerSaveDate();

            try {

                return myApplication.getAPI().getPointMap(managerSaveDate.loadAuthorizationData(context));

            } catch (IOException e) {

                return ERROR_CONNECT;
            }
        }

        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            Log.d(LOG_TAG, "responseString Map = " + strJson);

            if (strJson.equals(ERROR_CONNECT)) {

                Toast.makeText(getBaseContext(), getString(R.string.error_connection),
                        Toast.LENGTH_LONG).show();

                mpDialog.dismiss();

            } else {

                try {

                    map.clear();
                    pointMapList.clear();

                    JSONObject dataJsonObj = null;

                    dataJsonObj = new JSONObject(strJson);
                    JSONArray jobArray = dataJsonObj.getJSONArray(LIST_POINT_MAP);

                    for (int i = 0; i < jobArray.length(); i++) {

                        JSONObject jobObject = jobArray.getJSONObject(i);
                        pointMapList.add(new PointMapItem(
                                Integer.valueOf(jobObject.getString(ID)),
                                jobObject.getString(TITLE),
                                Double.valueOf(jobObject.getString(COORDINATE_X)),
                                Double.valueOf(jobObject.getString(COORDINATE_Y))
                        ));
                    }

                    setMarker();

                    mpDialog.dismiss();
                } catch (JSONException e) {

                    mpDialog.dismiss();
                }
            }
        }
    }
}

